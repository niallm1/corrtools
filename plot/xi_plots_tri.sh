#!/bin/bash

bin=xi_plot_tri.py
mode=run
mock=mock1
version=5
#twopt_dir=comb_xis_y1_v0
twopt_dir=xis_v${version}
twopt_meas=$twopt_dir/tpt_${mock}_v${version}_wcov.fits
#twopt_meas=$twopt_dir/two_pt_cov.fits
save_dir=$twopt_dir/xi_plots_${mock}
cov_ext=COV_GAUSSIAN
fmt=png


mkdir $save_dir
cov_file=$twopt_meas
#cos_dump=/home/maccrann/DES/BCC_tests/buzzard/cosmosis/real_space/buzzard_v1.2/runs/all2pt_bias_zbin_5mpc/1/test
cos_dump=buzzard_4y1_output
#/home/maccrann/DES/BCC_tests/buzzard/cosmosis/real_space/buzzard_4y1_v4/runs/all2pt_ztrue_5mpc_mock3/1/test
twopt_ext=wtheta
theory_dir=$cos_dump/galaxy_xi
#theory_dir2=cos_dump_fullsky/galaxy_xi
theory_pref=bin
save_to=wtheta_${mock}.${fmt}

cmd="python $bin $twopt_meas $twopt_ext --theory_dirs $theory_dir $theory_dir2 --theory_pref $theory_pref --cov_file $cov_file --cov_ext $cov_ext --save_to $save_dir/$save_to"
echo $cmd
if [ $mode == run ]
  then
    $cmd
fi

twopt_ext=gammat
theory_dir=$cos_dump/galaxy_shear_xi
theory_pref=bin
save_to=gammat_${mock}.${fmt}

cmd="python $bin $twopt_meas $twopt_ext --theory_dir $theory_dir --theory_pref $theory_pref --cov_file $cov_file --cov_ext $cov_ext --save_to $save_dir/$save_to"
echo $cmd
if [ $mode == run ]
  then
    $cmd
fi

twopt_ext=xip
theory_dir=$cos_dump/shear_xi
theory_pref=xiplus
save_to=xip_${mock}.${fmt}

cmd="python $bin $twopt_meas $twopt_ext --theory_dir $theory_dir --theory_pref $theory_pref --cov_file $cov_file --cov_ext $cov_ext --save_to $save_dir/$save_to"
echo $cmd
if [ $mode == run ]
  then
    $cmd
fi

twopt_ext=xim
theory_dir=$cos_dump/shear_xi
theory_pref=ximinus
auto_only=""
save_to=xim_${mock}.${fmt}

cmd="python $bin $twopt_meas $twopt_ext --theory_dir $theory_dir --theory_pref $theory_pref --cov_file $cov_file --cov_ext $cov_ext  --save_to $save_dir/$save_to"
echo $cmd
if [ $mode == run ]
  then
    $cmd
fi


