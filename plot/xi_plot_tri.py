import numpy as np
import pickle
import pylab
import argparse
import os
import matplotlib
import fitsio
import scipy.interpolate as interp
import matplotlib.gridspec as gridspec
matplotlib.rcParams['font.family']='serif'
matplotlib.rcParams['font.size']=14
matplotlib.rcParams['legend.fontsize']=12
matplotlib.rcParams['xtick.major.size'] = 7.0
matplotlib.rcParams['xtick.minor.size'] = 4.0
matplotlib.rcParams['ytick.major.size'] = 7.0
matplotlib.rcParams['ytick.minor.size'] = 4.0
matplotlib.rcParams['lines.linewidth'] = 1
matplotlib.rcParams['axes.linewidth'] = 1
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']

matplotlib.rcParams.update({'figure.autolayout': True})

def parse_args():
    import argparse
    description = 'xipm with jackknife'

    parser = argparse.ArgumentParser(description=description, add_help=True,
                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('xi_meas_file', type=str)
    parser.add_argument('xi_ext',type=str)
    parser.add_argument('--cov_file',type=str,default=None)
    parser.add_argument('--cov_ext',type=str,default=None)
    parser.add_argument('--theory_dirs',nargs='*',type=str,default=None)
    parser.add_argument('--theory_pref',type=str,default="xiplus")
    parser.add_argument('--linear_y',action='store_true',default=False)
    parser.add_argument('--auto_only',action='store_true',default=False)
    parser.add_argument('--src_gt_lens',action='store_true',default=False)
    parser.add_argument('--save_to',type=str,default=None)
    parser.add_argument('--frac_diff',action='store_true', default=False, help='plot ratio of data to theory')
    parser.add_argument('--theory_labels',type=str,nargs='*',default=None,help='label the theory lines')
    args=parser.parse_args()
    #theory labels can have spaces:
    if args.theory_labels:
        for i in range(len(args.theory_labels)):
            args.theory_labels[i] = args.theory_labels[i].replace("<space>"," ")
    return args

class Xi_interp(object):
    def __init__(self,thetas,Xis):
        if np.all(Xis>0):
            self.interp_func=interp.interp1d(np.log(thetas),np.log(Xis),bounds_error=False,fill_value=-np.inf)
            self.interp_type='loglog'
        elif np.all(Xis<0):
            self.interp_func=interp.interp1d(np.log(thetas),np.log(-Xis),bounds_error=False,fill_value=-np.inf)
            self.interp_type='minus_loglog'
        else:
            self.interp_func=interp.interp1d(np.log(thetas),Xis,bounds_error=False,fill_value=0.)
            self.interp_type="log_ell"

    def __call__(self,thetas):
        if self.interp_type=='loglog':
            xi=np.exp(self.interp_func(np.log(thetas)))
            xi[np.isnan(xi)]==0.
        elif self.interp_type=='minus_loglog':
            xi=-np.exp(self.interp_func(np.log(thetas)))
            xi[np.isnan(xi)]==0.
        else:
            assert self.interp_type=="log_ell"
            xi=self.interp_func(np.log(thetas))
            xi[np.isnan(xi)]==0.
        return xi                        



def main():

    args=parse_args()
    
    if args.frac_diff:

        assert args.theory_dirs is not None

    xi_data=fitsio.read(args.xi_meas_file,ext=args.xi_ext)
    xi_header=fitsio.read_header(args.xi_meas_file,ext=args.xi_ext)
    num_z_a,num_z_b=xi_header['N_ZBIN_1'],xi_header['N_ZBIN_2']
    xi_dict={}
    bin_pairs=[]
    
    plot_errs=False
    if args.cov_file is not None:
        plot_errs=True
        cov=fitsio.read(args.cov_file,ext=args.cov_ext)
        cov_head=fitsio.read_header(args.cov_file,ext=args.cov_ext)
        names,starts=[],[]
        for i in range(100):
            try:
                names.append(cov_head['NAME_%d'%i].strip())
                starts.append(int(cov_head['STRT_%d'%i]))
            except ValueError:
                pass
        print names
        xi_ind=names.index(args.xi_ext)
        xi_start=starts[xi_ind]
        try:
            cov=cov[xi_start:starts[xi_ind+1],xi_start:starts[xi_ind+1]]
        except IndexError:
            cov=cov[xi_start:,xi_start:]
        print cov.shape
        errs = np.sqrt(np.diag(cov))
        

    #fig=pylab.figure(figsize=(14,14))
    #colors=['b','g','r','gold','purple','brown','black','cyan','magenta']
    markers=['o','^','v','D','h','>','<']*10
    mfcs=[None]*7+['white']*7
    k=0

    #get bin pairs
    for bin_i in range(1,num_z_a+1):
        for bin_j in range(1,num_z_b+1):
            use=(xi_data['BIN1']==bin_i)*(xi_data['BIN2']==bin_j)
            if use.sum()==0:
                continue
            else:
                if args.auto_only:
                    if bin_i!=bin_j:
                        continue
                bin_pairs.append((bin_i,bin_j))

                        
 

    offsets=1+0.08*np.linspace(-1,1,len(bin_pairs))*len(bin_pairs)/9.
    ymin=99999
    ymax=-999999
    k=0
    #read data xi's
    data_dict={}
    for bin_i in range(1,num_z_a+1):
        for bin_j in range(1,num_z_b+1):
            if args.auto_only:
                if bin_i!=bin_j:
                    continue
            if args.src_gt_lens:
                if bin_j<bin_i:
                    continue

            data_dict[(bin_i,bin_j)]={}
            use=(xi_data['BIN1']==bin_i)*(xi_data['BIN2']==bin_j)
            if use.sum()==0:
                continue
            xi_i_j = xi_data['VALUE'][use]
            theta = xi_data['ANG'][use]
            data_dict[(bin_i,bin_j)]['xi']=xi_i_j
            data_dict[(bin_i,bin_j)]['theta']=theta
            if args.cov_file is not None:
                data_dict[(bin_i,bin_j)]['xi_err']=errs[use]
            k+=1

    #theta_min,theta_max=0.8*theta.min(),1.2*theta.max()
    theta_min, theta_max=1.-1.e-9,300.+1.e-9

    #read theory dict
    if args.theory_dirs is not None:
        theory_dicts=[]
        for ti,theory_dir in enumerate(args.theory_dirs):
            theory_dict={}
            theta=np.degrees(np.loadtxt(os.path.join(theory_dir,'theta.txt')))*60
            theta_use=np.where((theta>=theta_min)*(theta<=theta_max))[0]
        
            if ti==0:
                add_low,add_high=np.array([]),np.array([])
                if theta[theta_use[0]]>theta_min+2.e-9:
                    if theta_use[0]!=0:
                        add_low = np.array([theta_use[0]-1])
                if theta[theta_use[-1]]<theta_max-2.e-9:
                    if theta_use[-1]!=len(theta)-1:
                        add_high=np.array([theta_use[-1]+1])
                theta_use=np.concatenate((add_low,theta_use,add_high)).astype(int)

            theory_dict['THETA']=theta[theta_use]
            print theory_dir, theta[theta_use]
            print bin_pairs
            for i,j in bin_pairs:
                if args.auto_only:
                    if i!=j:
                        continue
                if args.src_gt_lens:
                    if j<i:
                        continue
                try:
                    xi=np.loadtxt(os.path.join(theory_dir, args.theory_pref+'_%d_%d.txt'%(i,j)))[theta_use]
                except IOError:
                    xi=np.loadtxt(os.path.join(theory_dir, args.theory_pref+'_%d_%d.txt'%(j,i)))[theta_use]
                theory_dict[(i,j)]=xi
            theory_dicts.append(theory_dict)

    #now do plotting
    if args.auto_only:
        gs = gridspec.GridSpec(1,num_z_a)  
        fig = pylab.figure(figsize=(num_z_a*4,4))
    else:
        gs = gridspec.GridSpec(num_z_a,num_z_b)
        fig = pylab.figure(figsize=(12,12))
    print num_z_a, num_z_b
    print bin_pairs
    axs=[]
    linestyles=['-','--',':','-','--',':']
    colors=['k','b','r','darkviolet','darkorange','lightseagreen']
    theory_lines=[]
    k=0
    for i,j in bin_pairs:
        if args.auto_only:
            ax = fig.add_subplot(gs[0,i-1])
        else:
            ax = fig.add_subplot(gs[i-1,j-1])
        axs.append(ax)
        theta_data = data_dict[(i,j)]['theta']
        xi_data = data_dict[(i,j)]['xi']
        xi_err = data_dict[(i,j)]['xi_err']
        ax.text(0.8,0.8,"%d,%d"%(i,j),ha='right',transform=ax.transAxes)
        if not args.frac_diff:
            if args.linear_y:
                y_data=xi_data*theta_data
                y_err = xi_err*theta_data
            else:
                y_data=xi_data
                y_err=xi_err
            if plot_errs:
                ax.errorbar(theta_data, y_data, yerr=y_err, fmt='o', color='k')
                ymax=max(ymax, (y_data+y_err).max())
            else:
                ax.plot(theta_data,xi_data,'o',color=c,label='%d,%d'%(bin_i,bin_j))
                ymax=max(ymax, xi_data.max())
            ymin=min(ymin, np.abs(y_data).min())
            if args.theory_dirs is not None:
                for it,theory_dict in enumerate(theory_dicts):
                    xi_theory=theory_dict[(i,j)]
                    if args.linear_y:
                        y_theory=xi_theory*theory_dict['THETA']
                    else:
                        y_theory=xi_theory
                    xi_theory_interp=Xi_interp(theory_dict['THETA'],xi_theory)(theta_data)
                    l,=ax.plot(theory_dict['THETA'],y_theory,color=colors[it],linestyle=linestyles[it])
                    if k==0:
                        theory_lines.append(l)
        else:
            xi_theory_base=theory_dicts[0][(i,j)]
            xi_theory_interp_func=Xi_interp(theory_dicts[0]['THETA'],xi_theory_base)
            xi_theory_interp=xi_theory_interp_func(theta_data)
            print i, 'xi_theory_interp',xi_theory_interp
            l,=ax.plot([theta_min,theta_max],[0.,0.],'k-',alpha=0.5,lw=2.)
            if k==0:
                theory_lines.append(l)
            if plot_errs:
                ax.errorbar(theta_data, xi_data/xi_theory_interp-1, 
                            yerr=data_dict[(i,j)]['xi_err']/xi_theory_interp,fmt='o',color='k')
            else:
                ax.plot(theta_data,xi_data/xi_theory_interp-1, fmt=marker, mfc='o')
            if len(theory_dicts)>1:
                for it,theory_dict in enumerate(theory_dicts[1:]):
                    if linestyles[it+1]==':':
                        lw=3.
                    else:
                        lw=2.
                    xi_theory=theory_dict[(i,j)]
                    print xi_theory/xi_theory_interp_func(theory_dict['THETA'])-1
                    l,=ax.plot(theory_dict['THETA'],xi_theory/xi_theory_interp_func(theory_dict['THETA'])-1,
                            color=colors[it+1],linestyle=linestyles[it+1],lw=lw)
                    if k==0:
                        theory_lines.append(l)
            k+=1

        ax.set_xscale('log')
        ax.set_xlim([theta_min,theta_max])
        ax.set_xlabel(r'$\theta\quad $(arcmin)')

    #theory legend
    if args.theory_labels:
        print theory_lines 
        print args.theory_labels
        assert len(theory_lines)==len(args.theory_labels)
        lgd = ax.legend(theory_lines, args.theory_labels, bbox_to_anchor=(1.0,1.0), ncol=3, loc='lower right')
        bbox_extra_artists=(lgd,)
    else:
        bbox_extra_artists=None
    for ax,bin_pair in zip(axs,bin_pairs): 
        if not args.frac_diff:
            ax.set_ylim([0.9*ymin,1.1*ymax])
            if not args.linear_y:
                ax.set_yscale('log',nonposy='clip')
        else:
            ax.set_ylim([-0.5,0.5])
            if bin_pair[0]==1:
                ax.set_ylabel(r'$\xi/\xi_{theory}-1$')

    #pylab.show()
    if args.save_to is not None:
        pylab.savefig(args.save_to, bbox_extra_artists=bbox_extra_artists, bbox_inches='tight')
    else:
        pylab.show()


if __name__=="__main__":
    main()

