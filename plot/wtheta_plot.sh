#!/bin/bash

bin=xi_plot_tri.py
mode=run
#twopt_dir=comb_xis_y1_v0
twopt_dir=xis_v5/
#twopt_meas=$twopt_dir/two_pt_wcov.fits
#twopt_meas=$twopt_dir/two_pt_cov.fits
twopt_meas=$twopt_dir/tpt_mean_v5_wcov.fits
save_dir=$twopt_dir/xi_plots_tri_fracdiff
cov_ext=COV_GAUSSIAN

mkdir $save_dir
#cov_file=$twopt_dir/two_pt_cov.fits
cov_file=$twopt_meas
#cos_dump=/home/maccrann/DES/BCC_tests/buzzard/cosmosis/real_space/buzzard_v1.2/runs/all2pt_bias_zbin_5mpc/1/test
#cos_dump=/home/maccrann/DES/BCC_tests/buzzard/cosmosis/real_space/buzzard_4y1_v2/runs/all2pt_bias_zbin_5mpc/1/test
cos_dump=cos_dump_base
twopt_ext=wtheta
theory_dir=$cos_dump/galaxy_xi
theory_dir2=wtheta_theory_test/cos_dumps/cos_dump_fullsky/galaxy_xi
theory_dir4=class_prediction/wtheta_dump/galaxy_xi
theory_dir3=wtheta_theory_test/cos_dumps/cos_dump_cp/galaxy_xi
#theory_dir5=cos_dump_nonlimber_norsd/galaxy_xi
#theory_dir6=class_prediction/wtheta_dump_nogr/galaxy_xi
#theory_dir5=cos_dump_nonlimber_nonlinear_norsd/galaxy_xi
theory_pref=bin
save_to=wtheta_auto.png

theory_dirs="$theory_dir2  $theory_dir3 $theory_dir4"
theory_labels="Limber<space>full-sky  C&P  CLASS" # C&P<space>(nonlinear,<space>no<space>RSD)"

cmd="python $bin $twopt_meas $twopt_ext --theory_dirs $theory_dirs  --theory_pref $theory_pref --cov_file $cov_file --cov_ext $cov_ext --save_to $save_dir/$save_to --auto_only --linear_y --frac_diff --theory_labels $theory_labels"
echo $cmd
if [ $mode == run ]
  then
    $cmd
fi
