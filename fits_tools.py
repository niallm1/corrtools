#Tools to save correlation function meausurements to fits files.
import fitsio
from . import corr_tools
from . import n_of_z_tools
import numpy as np

def get_bin_pairs(num_z_bins, num_z_bins2=None):
    bin_pairs=[]
    for i in range(num_z_bins):
        if num_z_bins2 is None:
            j_start,j_end=i,num_z_bins
        else:
            j_start,j_end=0,num_z_bins2
        for j in range(j_start,j_end):
            bin_pairs.append((i,j))

    return bin_pairs

class Corr_FITS(fitsio.FITS):

    def add_cov(self, extname, cov, header=None):
        """header should be a dictionary"""
        self.write(cov, extname=extname, header=header)

    def add_n_of_z(self, extname, nofz):
        """nofz should be a n_of_z_tools.NofZ object"""
        dtypes=[('z_lo','f8'),('z_hi','f8'),('z_mid','f8')]
        num_bins=len(nofz.bin_n_of_zs)
        for i in range(num_bins):
            dtypes.append(('bin_%d'%i, 'f8'))

        data = np.zeros(len(nofz.z_mids), dtype=dtypes)
        data['z_lo']=nofz.z_lo
        data['z_hi']=nofz.z_hi
        data['z_mid']=nofz.z_mids
        for i in range(num_bins):
            data['bin_%d'%i] = nofz.bin_n_of_zs[i]

        header = {}
        header['nbins']=num_bins

        self.write(data, header=header, extname=extname)

    def add_binned_cl(self, extname, cl_dict, num_z_bins, ell_bin_lims, num_z_bins2=None, cl_dict_keys=['EE'], nofz1_name='nz1', 
                      nofz2=None, nofz2_name='nz2',binning='log', cov_ext='cl_cov', extra_info=None):
        """ell_bin_lims gives the first ell for each of the n ell bins, with the last element being what would be the first
        ell in the n+1th bin"""
        
        header={}
        header['n_of_z_1']=nofz1_name
        header['n_of_z_2']=nofz2_name
        header['num_z_bins']=num_z_bins
        header['ell_min']=ell_bin_lims[0]
        header['ell_max']=ell_bin_lims[-1]-1
        header['n_ell_bins']=len(ell_bin_lims)-1
        header['ell_units'] = "l*(l+1)/2pi"
        header['cov_ext'] = cov_ext
        n_ell_bins = len(ell_bin_lims)-1

        #extra_info allows you to pass a dictinory to be added to the header
        if extra_info is not None:
            for key,val in extra_info.iteritems():
                header[key] = val

        
        bin_ells=[]
        for ell_lo,ell_hi in zip(ell_bin_lims[:-1],ell_bin_lims[1:]):
            ells=np.arange(ell_lo, ell_hi)
            bin_ells.append(ells)
        
        #want to make rec array with rows for measurement, theta_min, theta_max, and mean theta for each measurement
        dtypes=[('ell_lo','f8'),('ell_hi','f8'),('bin_1','i4'),('bin_2','i4'),('ell_mid','f8')]
        for key in cl_dict_keys:
            dtypes.append((key,'f8'))
        
        #column lengths should be n_ell_bins * num_z_bins * (num_z_bins + 1) / 2
        nrows = n_ell_bins * num_z_bins * (num_z_bins + 1) / 2
        column_data={}
        for d in dtypes:
            column_data[d[0]] = np.zeros(nrows)

        start_ind=0
        for i in range(num_z_bins):
            for j in range(i,num_z_bins):
                key=(i,j)
                inds = np.arange(start_ind, start_ind+n_ell_bins)
                column_data['bin_1'][inds] = np.ones(n_ell_bins) * i
                column_data['bin_2'][inds] = np.ones(n_ell_bins) * j
                column_data['ell_lo'][inds] = ell_bin_lims[:-1]
                column_data['ell_hi'][inds] = ell_bin_lims[1:]
                for cl_key in cl_dict_keys:
                    column_data[cl_key][inds] = cl_dict[key][cl_key]
                start_ind+=n_ell_bins

        data = np.zeros(nrows, dtype=dtypes)
        for d in dtypes:
            data[d[0]] = column_data[d[0]]

        print header
        print data
        
        self.write(data, header=header, extname=extname)


    def add_xipm(self, extname_xip, xi_dict, num_z_bins, theta_min, theta_max, nbins, sep_units, num_z_bins2=None, nofz1_name='nz1', nofz2_name=None,binning='log', cov_ext='xipm_cov', xipm_sep_ext=True, extra_info=None):
        
        header={}
        header['n_of_z_1']=nofz1_name
        if nofz2_name is None:
            header['n_of_z_2']=nofz1_name
        else:
            header['n_of_z_2']=nofz2_name
        header['num_z_bins']=num_z_bins
        header['sep_min']=theta_min
        header['sep_max']=theta_max
        header['n_sep_bins']=nbins
        header['sep_units']=sep_units
        header['cov_ext']=cov_ext
        header['theta_min']=theta_min
        header['theta_max']=theta_max
        bin_pairs=get_bin_pairs(num_z_bins, num_z_bins2)

        #extra_info allows you to pass a dictinory to be added to the header
        if extra_info is not None:
            for key,val in extra_info.iteritems():
                header[key] = val

        #want to make rec array with rows for measurement, theta_min, theta_max, and mean theta for each measurement
        dtypes=[('theta_lo','f8'),('theta_hi','f8'),('bin_1','i4'),('bin_2','i4'),('xip','f8'),('xim','f8'),('theta_mid','f8')]
        
        #column lengths should be nbins * num_z_bins * (num_z_bins + 1) / 2
        nrows = nbins * len(bin_pairs)
        column_data={}
        for d in dtypes:
            column_data[d[0]] = np.zeros(nrows)

        theta_bin_lims = np.logspace(np.log10(theta_min), np.log10(theta_max), nbins+1)
        theta_lo,theta_hi =  theta_bin_lims[:-1], theta_bin_lims[1:]

        start_ind=0
        for i in range(num_z_bins):
            for j in range(i,num_z_bins):
                key=(i,j)
                inds = np.arange(start_ind, start_ind+nbins)
                column_data['bin_1'][inds] = np.ones(nbins) * i
                column_data['bin_2'][inds] = np.ones(nbins) * j
                column_data['theta_lo'][inds] = theta_lo
                column_data['theta_hi'][inds] = theta_hi
                column_data['theta_mid'][inds] = xi_dict[key]['theta']
                column_data['xip'][inds] = xi_dict[key]['xip']
                column_data['xim'][inds] = xi_dict[key]['xim']
                start_ind+=nbins

        data = np.zeros(nrows, dtype=dtypes)
        for d in dtypes:
            data[d[0]] = column_data[d[0]]
        
        self.write(data, header=header, extname=extname)

    def add_xi(self, extname, xi_dict, num_z_bins, theta_min, theta_max, nbins, sep_units, xi_col='xi', nofz1_name='nz1', nofz2_name=None,  binning='log', cov_ext=None, extra_info=None, num_z_bins2=None):
        
        header={}
        header['n_of_z_1']=nofz1_name
        if nofz2_name is None:
            header['n_of_z_2']=nofz1_name
        else:
            header['n_of_z_2']=nofz2_name
        header['num_z_bins_1']=num_z_bins
        if num_z_bins2 is not None:
            header['num_z_bins_2']=num_z_bins2
        else:
            header['num_z_bins_2']=num_z_bins
        header['sep_min']=theta_min
        header['sep_max']=theta_max
        header['n_sep_bins']=nbins
        header['sep_units']=sep_units
        header['cov_ext']=cov_ext
        header['theta_min']=theta_min
        header['theta_max']=theta_max

        #extra_info allows you to pass a dictinory to be added to the header
        if extra_info is not None:
            for key,val in extra_info.iteritems():
                header[key] = val

        #want to make rec array with rows for measurement, theta_min, theta_max, and mean theta for each measurement
        dtypes=[('theta_lo','f8'),('theta_hi','f8'),('bin_1','i4'),('bin_2','i4'),('xi','f8'),('theta_mid','f8')]
        bin_pairs=get_bin_pairs(num_z_bins, num_z_bins2)
        
        #column lengths should be nbins * num_z_bins * (num_z_bins + 1) / 2
        nrows = nbins * len(bin_pairs)
        column_data={}
        for d in dtypes:
            column_data[d[0]] = np.zeros(nrows)

        theta_bin_lims = np.logspace(np.log10(theta_min), np.log10(theta_max), nbins+1)
        theta_lo,theta_hi =  theta_bin_lims[:-1], theta_bin_lims[1:]

        start_ind=0
        print bin_pairs
        for i,j in bin_pairs:
            key=(i,j)
            inds = np.arange(start_ind, start_ind+nbins)
            print 'inds',inds
            column_data['bin_1'][inds] = np.ones(nbins) * i
            column_data['bin_2'][inds] = np.ones(nbins) * j
            column_data['theta_lo'][inds] = theta_lo
            column_data['theta_hi'][inds] = theta_hi
            column_data['theta_mid'][inds] = xi_dict[key]['theta']
            column_data['xi'][inds] = xi_dict[key][xi_col]
            start_ind+=nbins

        data = np.zeros(nrows, dtype=dtypes)
        for d in dtypes:
            data[d[0]] = column_data[d[0]]
        
        self.write(data, header=header, extname=extname)
        
        
        
