import nmbot
import shutil
import numpy as np
import fitsio
import treecorr
import argparse
#import pylab
import os
import corrtools as corr_tools
import scipy.interpolate as interp
import twopoint
from os.path import join as pj
import healpix_util as hu
import healpy as hp

shape_col_dict={'g1':'EPSILON1',
                'g2':'EPSILON2',
                'dec':'DEC',
                'ra':'RA',
                'w':None}

pos_col_dict={  'dec':'DEC',
                'ra':'RA',
                'w':None,
                'zbin':'zbin'}
rand_col_dict={  'dec':'dec',
                'ra':'ra',
                'w':None,
                'zbin':'zbin'}

def parse_args():
    import argparse
    description = """Compute 3x2pt functions from a shape catalog, lens catalog and randoms catalog, and save into
                     a useful fits format, including the n(z)s.

                     The shape catalog (specified by the 'shape_cat' argument) should be pre-redshift binned 
                     (this is generally a large catalog, so redshift binning now would be slow) i.e. the catalog should have column 
                     named by the 'shape_zbin_colname' argument.  

                     The lens catalog ('pos_cat') can be pre-binned (in which case specify the 'pos_zbin_colname'), or you can use
                     the pos_min_max_z, pos_n_z_bins and pos_eq_zbin_width arguments to do this on the fly. The random catalog
                     will be treated equivalently to the lens catalog.
                     
                     A histogram n(z) file should be provided with the shape catalog.
                     If the lens catalog is pre-binned, then also provide a histogram n(z) file for this (using the pos_nofz_file
                     argument)."""

    parser = argparse.ArgumentParser(description=description, add_help=True,
                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('shape_cat', type=str, help='shape fits catalog')
    parser.add_argument('shape_mask_zbin_file', type=str)
    parser.add_argument('pos_cat', type=str, help='position fits catalog')
    parser.add_argument('random_cat',type=str,help='random points fits catalog')
    parser.add_argument('outfile',type=str, help="output 2pt data file")
    parser.add_argument('--shape_nofz_files',type=str,nargs='*',help="n(z) histogram file for shapes - need this to save to fits measurement file")
    parser.add_argument('--shape_nofz_names',type=str,nargs='*',help="shape n(z) names")
    parser.add_argument('--pos_nofz_file',type=str,help="n(z) histogram file for lenses - need this to save to fits measurement file",default=None)
    parser.add_argument('--pos_mask',type=str,help="mask to apply to position sample",default=None)
    parser.add_argument('--pos_zbin_colname', type=str, default=None,help="If position sample already z-binned, this column contains the bin indices")
    parser.add_argument('--shape_zbin_colname',type=str,default='zbin_mean_z', help="same but for sources")
    parser.add_argument('--GG_nbins',type=int,default=20)
    parser.add_argument('--GG_min_max_theta',nargs=2,type=float, default=[2.,300.])
    parser.add_argument('--nn_nbins',type=int,default=20)
    parser.add_argument('--nn_min_max_theta',nargs=2,type=float, default=[2.,300.])
    parser.add_argument('--nG_nbins',type=int,default=20)
    parser.add_argument('--nG_min_max_theta',nargs=2,type=float, default=[2.,300.])
    parser.add_argument('--use_shear_col',action='store_true',default=False)
    parser.add_argument('--reduced_shear',action='store_true',default=False)
    parser.add_argument('--pos_min_max_z',nargs=2,type=float,default=None)
    parser.add_argument('--pos_n_z_bins',type=int,default=1)
    parser.add_argument('--pos_zcolname',type=str,default='ZSPEC', help="If position sample not already z-binned, use this column to bin by")
    parser.add_argument('--pos_truth_zcolname',type=str,default=None, help="If position sample not already z-binned, and true redshifts are known, use this colum to construct n(z)s")
    parser.add_argument('--pos_zerrcolname',type=str,default=None, help="If position sample true z unknown, assume Gaussian z erros with this column as the sigma")
    parser.add_argument('--bin_slop_rec',type=int,default=4, help='Set bin_slop to the recommended value multiplied by this value')
    parser.add_argument('--bin_slop',type=float,default=None)
    parser.add_argument('--pos_eq_zbin_width', default=False, action='store_true')
    args=parser.parse_args()
    return args


def main():

    args=parse_args()        
    save_dir=os.path.split(args.outfile)[0]
    if not os.path.isdir(save_dir):
        try:
            os.makedirs(save_dir)
        except OSError:
            pass

    if args.shape_nofz_names:
        assert len(args.shape_nofz_names)==len(args.shape_nofz_files)

    if args.use_shear_col:
        sh_quantities=('g1','g2')
    else:
        sh_quantities=('e1','e2')

    #Read in catalogs
    shape_fits=fitsio.FITS(args.shape_cat)
    shape_columns = [sh_quantities[0],sh_quantities[1],'weight']
    shape_data=shape_fits[1].read(columns=shape_columns)
    gold_data=fitsio.read(args.shape_cat.replace('shape','gold'), columns=['ra','dec'])
    shape_mask_zbin_data=fitsio.read(args.shape_mask_zbin_file)
    use = shape_mask_zbin_data['mask']==1
    shape_data, shape_mask_zbin_data, gold_data = shape_data[use], shape_mask_zbin_data[use], gold_data[use]
    shape_data = nmbot.add_field(shape_data, [(args.shape_zbin_colname, 'int'), ('ra',float),('dec',float)], 
                                 [shape_mask_zbin_data[args.shape_zbin_colname],gold_data['ra'],gold_data['dec']])
    print '%d shapes'%shape_data.shape

    #Read in pos catalog
    pos_data=fitsio.read(args.pos_cat)
    #Read in random catalog
    rand_data=fitsio.read(args.random_cat)

    #Set up gg correlator
    bin_slop_base=0.05/((np.log(args.GG_min_max_theta[1])-np.log(args.GG_min_max_theta[0]))/args.GG_nbins)
    if args.bin_slop:
        bin_slop=args.bin_slop
    else:
        bin_slop=args.bin_slop_rec*bin_slop_base
    print "using bin_slop=%f, which is %d times the recommended bin_slop"%(bin_slop, bin_slop/bin_slop_base)

    #Set up gg correlator
    nbins=args.GG_nbins                                                                                  
    min_sep=args.GG_min_max_theta[0]                                                                     
    max_sep=args.GG_min_max_theta[1]                                                                   
    sep_units='arcmin'
    GG=treecorr.GGCorrelation(nbins=nbins, min_sep=min_sep, max_sep=max_sep, sep_units=sep_units,verbose=1,bin_slop=bin_slop)

    #Set up position-position correlators
    nbins=args.nn_nbins                                                                            
    min_sep=args.nn_min_max_theta[0]                                                                     
    max_sep=args.nn_min_max_theta[1]                                                                   
    sep_units='arcmin'
    nn=treecorr.NNCorrelation(nbins=nbins, min_sep=min_sep, max_sep=max_sep, sep_units=sep_units,verbose=1,bin_slop=bin_slop)

    #Set up position shear correlator
    sep_units='arcmin'
    nG=treecorr.NGCorrelation(nbins=args.nG_nbins, min_sep=args.nG_min_max_theta[0], 
                              max_sep=args.nG_min_max_theta[1], sep_units=sep_units,verbose=1,bin_slop=bin_slop)

    if args.use_shear_col or args.reduced_shear:
        shape_col_dict['g1']='GAMMA1'
        shape_col_dict['g2']='GAMMA2'
    if args.reduced_shear:
        shape_data['GAMMA1']/=(1-shape_data['KAPPA'])
        shape_data['GAMMA2']/=(1-shape_data['KAPPA'])

    #Make catalogs, shapes:
    print 'making source GalCat'
    shape_cat_full=corr_tools.GalCat(shape_data, x_col='ra', y_col='dec', quantities=[sh_quantities], 
                                     zbin_col_name=args.shape_zbin_colname, exclude_zbin=-1, flip=(sh_quantities[1],))

    #positions:
    if args.pos_min_max_z:
        assert args.pos_mask
    else:
        args.pos_min_max_z=[None,None]

    print 'making lens GalCat'
    pos_cat_full=corr_tools.GalCat(pos_data, x_col='RA', y_col='DEC', zbin_col_name=args.pos_zbin_colname, exclude_zbin=-1)
    if not pos_cat_full.redshift_split_done:
        pos_cat_full.redshift_split(zcol=args.pos_zcolname, zmin=args.pos_min_max_z[0], zmax=args.pos_min_max_z[1], 
                                    n_split=args.pos_n_z_bins, equal_bin_width=args.pos_eq_zbin_width)
    print 'making rand GalCat'
    rand_cat_full=corr_tools.GalCat(rand_data, x_col='ra', y_col='dec', zbin_col_name=args.pos_zbin_colname, exclude_zbin=-1)
    if not rand_cat_full.redshift_split_done:
        rand_cat_full.redshift_split(zcol=args.pos_zcolname, zmin=args.pos_min_max_z[0], zmax=args.pos_min_max_z[1], 
                                     n_split=args.pos_n_z_bins, equal_bin_width=args.pos_eq_zbin_width)
    
    #n(z) stuff
    z_lims=None
    shape_nzs=[]
    for f in args.shape_nofz_files:
        shape_nz=corr_tools.DES_NofZ(hist_file=f)
        z_lims=shape_nz.z_lims
        shape_nzs.append(shape_nz)

    if args.pos_nofz_file is not None:
        pos_nz=corr_tools.DES_NofZ(hist_file=args.pos_nofz_file)

    else:
        if args.pos_truth_zcolname is None:
            pos_nz=corr_tools.get_nofzs(pos_cat_full.arr_data, pos_cat_full.zbin_column, args.pos_zcolname, nz_lims=z_lims,
                                         zerrcol=args.pos_zerrcolname)
        else:
            pos_nz=corr_tools.get_nofzs(pos_cat_full.arr_data, pos_cat_full.zbin_column, args.pos_truth_zcolname, nz_lims=z_lims)
        pos_nz = corr_tools.DES_NofZ(z_lims=pos_nz.z_lims, bin_n_of_zs=pos_nz.bin_n_of_zs, total_n_of_z=np.zeros_like(poz_nz.z_mids))

    #Do the measurements!
    wthetaer = corr_tools.CorrMeasurement(pos_cat_full, 'count', XX=nn, rand_cat1=rand_cat_full)
    wtheta_spec = wthetaer.get_spec('wtheta', 'nz_pos')
    xipmer = corr_tools.CorrMeasurement(shape_cat_full, sh_quantities, XX=GG)
    xipm_specs = xipmer.get_spec(['xip','xim'], 'nz_shape')
    gtaner = corr_tools.CorrMeasurement(pos_cat_full, 'count', gal_cat2=shape_cat_full,q2= sh_quantities,rand_cat1 = rand_cat_full,XX=nG)
    gtan_spec = gtaner.get_spec('gammat', 'nz_pos', kernel_name2 = 'nz_shape')
    print 'specs:', xipm_specs, wtheta_spec, gtan_spec
    #specs = xipm_specs+wtheta_spec+gtan_spec
    specs = xipm_specs+gtan_spec+wtheta_spec

    #Save outputs
    #now putting sigma_e, N_gal in kernels...get these from catalog bin_stats
    sigma_e1e2 = shape_cat_full.bin_stats['sigma'][sh_quantities]
    sigma_e = [0.5*(s1+s2) for (s1,s2) in sigma_e1e2]
    N_gal_shape = shape_cat_full.bin_stats['N_obj']
    N_gal_pos = pos_cat_full.bin_stats['N_obj']
    shape_nz_kernels=[]
    print 'creating n(z) extensions'
    for si,s in enumerate(shape_nzs):
        if args.shape_nofz_names:
            name="nz_"+args.shape_nofz_names[si]
        else:
            if si==0:
                name="nz_shape"
            else:
                name="nz_shape"+"_%d"%si
        print 'name, N_gal, sigma_e:',name, N_gal_shape, sigma_e
        shape_nz_kernel = twopoint.NumberDensity(name,s.z_lo,s.z_mids,s.z_hi,s.bin_n_of_zs,
                                                 ngal=N_gal_shape, sigma_e=sigma_e)
        shape_nz_kernels.append(shape_nz_kernel)
    pos_nz_kernel = twopoint.NumberDensity("nz_pos",pos_nz.z_lo,pos_nz.z_mids,pos_nz.z_hi,pos_nz.bin_n_of_zs,
                                           ngal=N_gal_pos)

    tpt_file = twopoint.TwoPointFile(specs,shape_nz_kernels+[pos_nz_kernel],None,None)
    tpt_file.to_fits(pj(args.outfile),clobber=True)

    return 0

if __name__=="__main__":
    main()
