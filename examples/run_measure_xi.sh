#!/bin/bash
#This is a (not very widely applicable) example script
#It's for running 3x2pt xi measurements on the buzzard sims at nersc
#But hopefully looking at measure_xis.py will help with usage of what's
#in corr_tools.py.

source /global/cscratch1/sd/zuntz/stack/setup
module unload mpi4py-hpcp
export PYTHONPATH=$PYTHONPATH:/global//homes/m/maccrann/code/my_python/2point

#Define some useful paths/variables here
mock_dir_scratch=/global/cscratch1/sd/maccrann/DES/BCC/mock1_6
buzzard_dir=/global/project/projectdirs/des/jderose/addgals/catalogs/Buzzard/Catalog_v1.1/four-y1-mocks/mock1

#compute xis.
bin=measure_xis.py
shape_cat=${buzzard_dir}/mergedcats/Buzzard_v1.1_1_shape.fits.gz
shape_mask_zbin_file=${mock_dir_scratch}/shape_mask_zbin_info_3z.fits
#shape_mask=$mock_dir_scratch/shape_mask.fits
outfile=tpt_test.fits
shape_nzs="$mock_dir_scratch/nz_shape_mean_z_z_mc.hist $mock_dir_scratch/nz_shape_mean_z_redshift.hist"
shape_nz_names="bpz true"
shape_zbincol=zbin_mean_z
pos_zbin_colname=zbin
pos_cat=$mock_dir_scratch/pos_data.fits
rand_cat=$mock_dir_scratch/rand_data.fits
pos_nz=$mock_dir_scratch/nz_redmagic.hist
theta_min=2.
theta_max=300.
ntheta=20
bin_slop=1.
cmd="python $bin $shape_cat $shape_mask_zbin_file $pos_cat $rand_cat $outfile --shape_nofz_files $shape_nzs \
     --shape_zbin_colname $shape_zbincol --pos_zbin_colname $pos_zbin_colname  --bin_slop $bin_slop \
     --pos_nofz_file $pos_nz --GG_nbins $ntheta --GG_min_max_theta $theta_min $theta_max --nG_nbins $ntheta \
     --nG_min_max_theta $theta_min $theta_max --nn_nbins $ntheta --nn_min_max_theta $theta_min $theta_max"
date
echo $cmd
$cmd
