import numpy as np

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))/sig/np.sqrt(2*np.pi)

def get_nofzs(gal_data, zbin_column, zcol, nz_lims=None, zerrcol=None):
    if nz_lims is None:
        nz_lims = np.linspace(0.,4.,401)
    nz_mids = 0.5*(nz_lims[:-1]+nz_lims[1:])
    zbins=np.sort(np.unique(zbin_column))
    nzs=[]
    for zbin in zbins:
        use=(zbin_column==zbin)
        zs_use=gal_data[zcol][use]
        if zerrcol is None:
            nz,_=np.histogram(zs_use,bins=nz_lims)
        else:
            nz = np.zeros(len(nz_mids))
            for z,zerr in zip(zs_use,gal_data[zerrcol][use]):
                nz_single=gaussian(nz_mids, z, zerr)
                nz+=nz_single
        nzs.append(nz)
        
    return NofZ(nz_lims, nzs)


class NofZ(object):
    def __init__(self, z_lims, n_of_zs):
        self.z_lims=z_lims
        self.z_lo=z_lims[:-1]
        self.z_hi=z_lims[1:]
        self.dzs=self.z_hi-self.z_lo
        self.z_mids=0.5*(self.z_lo+self.z_hi)
        self.bin_n_of_zs=np.array(n_of_zs)

    def save_to_cosmosis_fmt(self,outfile):
        to_save=np.vstack((self.z_mids,self.bin_n_of_zs))
        np.savetxt(outfile,to_save.T)
                        
    def normalize(self,n_gals_in_bin=None):
        """
        normalize (or unnormalise i guess) n(z)s to have n_gals_in_bin[i]
        galaxies in bin[i]. if n_gals_in_bin is None, normalize so that \Sum z[i]*n[i] = 1
        """
        if n_gals_in_bin is None:
            n_gals_in_bin=[1]*self.n_z_bins
        assert len(n_gals_in_bin)==self.n_z_bins
        for i in range(self.n_z_bins):
            self.bin_n_of_zs[i]=n_gals_in_bin[i]*self.bin_n_of_zs[i]/np.sum(self.dzs*self.bin_n_of_zs[i])


class DES_NofZ(NofZ):
    def __init__(self, hist_file=None, z_lims=None, bin_n_of_zs=None, total_n_of_z=None, no_tomo=False):
        if hist_file is not None:
            self.init_from_file(hist_file,no_tomo=no_tomo)
        elif None in (z_lims, bin_n_of_zs, total_n_of_z):
            raise NameError
        else:
            self.init_from_arrays(z_lims, bin_n_of_zs, total_n_of_z)

    def init_from_arrays(self, z_lims, bin_n_of_zs, total_n_of_z):
        self.z_lims=z_lims
        self.z_lo=z_lims[:-1]
        self.z_hi=z_lims[1:]
        self.dzs=self.z_hi-self.z_lo
        self.bin_n_of_zs=np.array(bin_n_of_zs)
        self.n_of_z_total=np.array(total_n_of_z)
        self.z_mids=0.5*(self.z_lo+self.z_hi)

    def init_from_file(self,hist_file, no_tomo=False):
        """
        histogram file has the following columns:
        z_lo, z_hi, n(z_0),...n(z_n), n(z_total)
        where n is the number of redshift bins
        """
        z_data=np.loadtxt(hist_file).T
        self.z_lo=z_data[0]
        self.z_hi=z_data[1]
        self.z_lims=np.zeros(len(z_data[0])+1)
        self.z_lims[:-1]=z_data[0]
        self.z_lims[-1]=z_data[1][-1]
        self.dzs=self.z_hi-self.z_lo
        self.z_mids=0.5*(z_data[0]+z_data[1])
        self.n_of_z_total=z_data[-1]
        if not no_tomo:
            self.bin_n_of_zs=z_data[2:-1]
            self.n_z_bins=self.bin_n_of_zs.shape[0]
            print "created des_nof_z with %d redshift bins"%self.n_z_bins
        else:
            self.bin_n_of_zs=z_data[-1:]
            self.n_z_bins=self.bin_n_of_zs.shape[0]
            print "created des_nof_z with %d redshift bins"%self.n_z_bins

    def save_to_cosmosis_fmt(self,outfile):
        to_save=np.vstack((self.z_lo,self.z_hi,self.bin_n_of_zs,self.n_of_z_total))
        np.savetxt(outfile,to_save.T)
