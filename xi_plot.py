import numpy as np
import pickle
import pylab
import argparse
import os
import matplotlib
import fitsio
import scipy.interpolate as interp
matplotlib.rcParams['font.family']='serif'
matplotlib.rcParams['font.size']=14
matplotlib.rcParams['legend.fontsize']=12
matplotlib.rcParams['xtick.major.size'] = 7.0
matplotlib.rcParams['xtick.minor.size'] = 4.0
matplotlib.rcParams['ytick.major.size'] = 7.0
matplotlib.rcParams['ytick.minor.size'] = 4.0
matplotlib.rcParams['lines.linewidth'] = 1
matplotlib.rcParams['axes.linewidth'] = 1
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']

matplotlib.rcParams.update({'figure.autolayout': True})

def parse_args():
    import argparse
    description = 'xipm with jackknife'

    parser = argparse.ArgumentParser(description=description, add_help=True,
                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('xi_meas_file', type=str)
    parser.add_argument('xi_ext',type=str)
    parser.add_argument('--cov_file',type=str,default=None)
    parser.add_argument('--cov_ext',type=str,default=None)
    parser.add_argument('--theory_dir',type=str,default=None)
    parser.add_argument('--theory_pref',type=str,default="xiplus")
    parser.add_argument('--linear_y',action='store_true',default=False)
    parser.add_argument('--auto_only',action='store_true',default=False)
    parser.add_argument('--src_gt_lens',action='store_true',default=False)
    parser.add_argument('--save_to',type=str,default=None)
    parser.add_argument('--frac_diff',action='store_true', default=False, help='plot ratio of data to theory')
    args=parser.parse_args()
    return args

class Xi_interp(object):
    def __init__(self,thetas,Xis):
        if np.all(Xis>0):
            self.interp_func=interp.interp1d(np.log(thetas),np.log(Xis),bounds_error=False,fill_value=-np.inf)
            self.interp_type='loglog'
        elif np.all(Xis<0):
            self.interp_func=interp.interp1d(np.log(thetas),np.log(-Xis),bounds_error=False,fill_value=-np.inf)
            self.interp_type='minus_loglog'
        else:
            self.interp_func=interp.interp1d(np.log(thetas),Xis,bounds_error=False,fill_value=0.)
            self.interp_type="log_ell"

    def __call__(self,thetas):
        if self.interp_type=='loglog':
            xi=np.exp(self.interp_func(np.log(thetas)))
            xi[np.isnan(xi)]==0.
        elif self.interp_type=='minus_loglog':
            xi=-np.exp(self.interp_func(np.log(thetas)))
            xi[np.isnan(xi)]==0.
        else:
            assert self.interp_type=="log_ell"
            xi=self.interp_func(np.log(thetas))
            xi[np.isnan(xi)]==0.
        return xi                        



def main():

    args=parse_args()
    
    if args.frac_diff:

        assert args.theory_dir is not None

    xi_data=fitsio.read(args.xi_meas_file,ext=args.xi_ext)
    print xi_data
    xi_header=fitsio.read_header(args.xi_meas_file,ext=args.xi_ext)
    print xi_header
    num_z_a,num_z_b=xi_header['NUM_Z_BINS_1'],xi_header['NUM_Z_BINS_2']
    xi_dict={}
    bin_pairs=[]
    
    plot_errs=False
    if args.cov_file is not None:
        plot_errs=True
        cov=fitsio.read(args.cov_file,ext=args.cov_ext)

    fig=pylab.figure()
    ax=fig.add_subplot(111)
    colors=['b','g','r','gold','purple','brown','black','cyan','magenta']
    markers=['o','^','v','D','h','>','<']*10
    mfcs=[None]*7+['white']*7
    k=0

    for bin_i in range(num_z_a):
        for bin_j in range(num_z_b):
            if args.auto_only:
                if bin_i!=bin_j:
                    continue
            if args.src_gt_lens:
                if bin_j<bin_i:
                    continue            
            use=(xi_data['bin_1']==bin_i)*(xi_data['bin_2']==bin_j)
            if use.sum()==0:
                continue
            else:
                bin_pairs.append((bin_i,bin_j))

    offsets=1+0.05*np.linspace(-1,1,len(bin_pairs))*len(bin_pairs)/9.
    ymin=99999
    ymax=-999999
    k=0
    data_dict={}
    for bin_i in range(num_z_a):
        for bin_j in range(num_z_b):
            if args.auto_only:
                if bin_i!=bin_j:
                    continue
            if args.src_gt_lens:
                if bin_j<bin_i:
                    continue

            data_dict[(bin_i,bin_j)]={}
            c,marker,mfc=colors[k],markers[k],mfcs[k]
            use=(xi_data['bin_1']==bin_i)*(xi_data['bin_2']==bin_j)
            if use.sum()==0:
                continue
            xi_i_j = xi_data['xi'][use]
            theta = xi_data['theta_mid'][use]
            data_dict[(bin_i,bin_j)]['xi']=xi_i_j
            data_dict[(bin_i,bin_j)]['theta']=theta
            if plot_errs:
                errs=np.sqrt(cov[use,use])
                data_dict[(bin_i,bin_j)]['xi_err']=errs
                if not args.frac_diff:
                    ax.errorbar(theta*offsets[k], xi_i_j, yerr=errs, color=c, fmt=marker,label='%d,%d'%(bin_i,bin_j),mfc=mfc)
                ymax=max(ymax, (xi_i_j+errs).max())
            else:
                if not args.frac_diff:
                    ax.plot(theta,xi_i_j,'o',color=c,label='%d,%d'%(bin_i,bin_j))
                ymax=max(ymax, xi_i_j.max())
            ymin=min(ymin, np.abs(xi_i_j).min())
            k+=1

    theta_min,theta_max=0.8*theta.min(),1.2*theta.max()

    if args.theory_dir is not None:
        theory_dict={}
        theta=np.degrees(np.loadtxt(os.path.join(args.theory_dir,'theta.txt')))*60
        theta_use=(theta>theta_min)*(theta<theta_max)
        theory_dict['theta']=theta[theta_use]
        k=0
        for i,j in bin_pairs:
            if args.auto_only:
                if i!=j:
                    continue
            if args.src_gt_lens:
                if j<i:
                    continue
            c=colors[k]
            xi=np.loadtxt(os.path.join(args.theory_dir, args.theory_pref+'_%d_%d.txt'%(i+1,j+1)))[theta_use]
            theory_dict[(i,j)]=xi
            if not args.frac_diff:
                ax.plot(theta[theta_use],xi,color=c)
            k+=1

    if args.frac_diff:
        k=0
        for i,j in bin_pairs:
            if args.auto_only:
                if i!=j:
                    continue
            if args.src_gt_lens:
                if j<i:
                    continue

            c,marker,mfc=colors[k],markers[k],mfcs[k]
            theta_data=data_dict[(i,j)]['theta']
            xi_data=data_dict[(i,j)]['xi']
            xi_theory=theory_dict[(i,j)]
            xi_theory_interp=Xi_interp(theory_dict['theta'],xi_theory)(theta_data)
            if plot_errs:
                ax.errorbar(offsets[k]*theta_data, xi_data/xi_theory_interp-1, yerr=data_dict[(i,j)]['xi_err']/xi_theory_interp, fmt=marker, color=c, mfc=mfc, label='%d,%d'%(i,j))
            else:
                ax.plot(offsets[k]*theta_data,xi_data/xi_theory_interp-1, fmt=marker, mfc=mfc, color=c, label='%d,%d'%(i,j))
            k+=1


    ax.set_xscale('log')
    ax.set_xlim([theta_min,theta_max])
    ax.set_xlabel(r'$\theta $(arcmin)')
    print ymin,ymax
    if not args.frac_diff:
        ax.set_ylim([ymin,ymax])
        ax.set_yscale('log',nonposy='clip')
    else:
        ax.plot([theta_min,theta_max],[0.,0.],'k--',alpha=0.5)
        ax.set_ylim([-0.5,0.5])
        ax.set_ylabel(r'$\xi/\xi_{theory}-1$')

    ax.legend()
    if args.save_to is not None:
        pylab.savefig(args.save_to)
    else:
        pylab.show()


if __name__=="__main__":
    main()

