import nmbot
import copy
import numpy as np
import fitsio
import treecorr
from numpy.lib.recfunctions import append_fields
import twopoint
import astropy.units

shape_data_keys=['e1','e2','ra','dec','weight','zbin']
DEFAULT_SHAPE_DATA_DICT={}
for key in shape_data_keys:
    DEFAULT_SHAPE_DATA_DICT[key]=key

def rad2arcmin(x):
    return 60.*np.degrees(x)

def treecorr_correlation2dict(XX,sep_units='arcmin'):
    XX_dict={}
    min_sep_rad, max_sep_rad = XX.min_sep*XX.sep_units, XX.max_sep*XX.sep_units
    XX_dict['min_sep']=rad2arcmin(min_sep_rad)
    XX_dict['max_sep']=rad2arcmin(max_sep_rad)
    XX_dict['nbins']=XX.nbins
    XX_dict['sep_units']='arcmin'
    XX_dict['bin_slop']=XX.bin_slop
    return XX_dict

def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- Np ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    variance = np.average((values-average)**2, weights=weights)  # Fast and numerically precise
    return (average, np.sqrt(variance))

def sample_cov(xi_arrays,mode='full'):
    """mode should be full, subsample or jk"""
    nsample,npoints=xi_arrays.shape
    Cov=np.zeros((npoints,npoints))
    Corr=np.zeros_like(Cov)

    xi_mean=np.mean(xi_arrays, axis=0)
    for i in range(npoints):
        for j in range(npoints):
            Cov[i,j]=np.sum((xi_arrays[:,i]-xi_mean[i])*(xi_arrays[:,j]-xi_mean[j]))/nsample
    #This is the covariance in a patch of size A/nsample. Assume Cov ~ 1/A so Cov=Cov/nsample
    if mode=='subsample':
        Cov /= nsample
    elif mode=="jk":
        Cov *= (nsample-1)
    for i in range(npoints):
        for j in range(npoints):
            Corr[i,j] = Cov[i,j]/np.sqrt(Cov[i,i]*Cov[j,j])  
    return Cov, Corr

class GalCat(object):

    def __init__(self, arr_data, x_col='ra', y_col='dec', r_col=None, w_col=None, x_units='deg', y_units='deg', r_units=None, zbin_col_name=None,
                 exclude_zbin=-1, subpatch_col=None, quantities=None, flip=None):

        """quantities is a list of column names (for spin 0 quantities) or tuples of column names (for spin 2). E.g. ['kappa',('e1','e2')],
        where arr_data needs to have these columns"""
        self.arr_data=arr_data
        self.redshift_split_done=False
        if quantities:
            self.quantities=quantities
        else:
            self.quantities=[]
        self.spin=[]
        for q in self.quantities:
            if isinstance(q, basestring):
                self.spin.append(0)
            elif isinstance(q, tuple):
                self.spin.append(2)
        if flip:
            for f in flip:
                self.arr_data[f]*=-1

        self.x_col, self.y_col, self.r_col = x_col, y_col, r_col
        self.x_units, self.y_units = x_units, y_units
        if w_col:
            self.w_col=w_col
        else:
            self.w_col='no_weight'
            if self.w_col not in self.arr_data.dtype.names:
                self.arr_data=nmbot.add_field(self.arr_data,[(self.w_col,float),],[np.ones(len(self.arr_data))])

        self.zbin_col_name=zbin_col_name
        if self.zbin_col_name is not None:
            self.redshift_split(zbin_col_name=self.zbin_col_name,exclude_zbin=exclude_zbin)
        else:
            self.zbin_column=None

        self.subpatch_col=subpatch_col
        if self.subpatch_col:
            self.patch_nums = np.unique(self.arr_data[subpatch_col])

        self.tc_cats={}

    def set_weights(self, weights, weight_col_name='w'):
        self.w_col=weight_col_name
        self.arr_data=append_fields(self.arr_data, [weight_col_name], [weights])
    
    def get_bin_stats(self,verbose=False):
        """useful to store the number of objects in each bin, and sigma e in each bin for e.g. covairance calculations"""
        self.bin_stats={}
        self.bin_stats['N_obj']=[]
        self.bin_stats['sigma']={}
        self.bin_stats['mean']={}
        for q in self.quantities:
            self.bin_stats['sigma'][q]=[]
            self.bin_stats['mean'][q]=[]

        for zbin in self.zbin_col_vals:
            #try and get weights
            zbin_mask=self.zbin_masks[zbin]
            if self.w_col is not None:
                try:
                    weights=self.arr_data[self.w_col][zbin_mask]
                except AttributeError:
                    weights=np.ones(zbin_mask.sum())
            else:
                weights=np.ones(zbin_mask.sum())
            N_obj=zbin_mask.sum()
            self.bin_stats['N_obj'].append(N_obj)
            print "N_obj",N_obj
            for q,s in zip(self.quantities,self.spin):
                if s==0:
                    mean_q, sigma_q = weighted_avg_and_std(self.arr_data[q][zbin_mask], weights)
                    sigma_q *= np.sqrt(np.sum(weights**2)/np.sum(weights)**2) * np.sqrt(N_obj)
                    self.bin_stats['sigma'][q].append(sigma_q)
                    self.bin_stats['mean'][q].append(mean_q)
                else:
                    mean_q1, sigma_q1 = weighted_avg_and_std(self.arr_data[q[0]][zbin_mask], weights)
                    sigma_q1 *= np.sqrt(np.sum(weights**2)/np.sum(weights)**2) * np.sqrt(N_obj)
                    mean_q2, sigma_q2 = weighted_avg_and_std(self.arr_data[q[1]][zbin_mask], weights)
                    sigma_q2 *= np.sqrt(np.sum(weights**2)/np.sum(weights)**2) * np.sqrt(N_obj)
                    self.bin_stats['sigma'][q].append((sigma_q1,sigma_q2))
                    self.bin_stats['mean'][q].append((mean_q1,mean_q2))

        if verbose:
            print "self.bin_stats",self.bin_stats

    def get_zbin_subpatch_cols(self):
        if self.subpatch_col:
            if self.zbin_column is None:
                return [self.arr_data[self.subpatch_col]]
            else:
                #need to split by redshift
                col_list=[]
                for m in self.zbin_masks:
                    arr_data_use=self.arr_data[m]
                    col_list.append(arr_data_use[self.subpatch_col])
                return col_list
        else:
            return None

    def get_all_tc_cats(self, sub_mean=False):
        for q in self.quantities:
            get_tc_cats(self, q, sub_mean=sub_mean)

    def get_tc_cats(self, quantity, sub_mean=False):
        assert ((quantity=='count') or (quantity in self.quantities))
        try:
            return self.tc_cats[quantity]
        except KeyError:
            if quantity=='count':
                get_cat=self.get_count_cat
            elif self.spin[self.quantities.index(quantity)]==0:
                get_cat=self.get_kappa_cat
            else:
                get_cat=self.get_shear_cat

            if self.zbin_column is None:
                tc_cats=[get_cat(self.arr_data, quantity, sub_mean=sub_mean)]
            else:
                #need to split by redshift
                tc_cats=[]
                for m in self.zbin_masks:
                    arr_data_use=self.arr_data[m]
                    tc_cats.append(get_cat(arr_data_use, quantity, sub_mean=sub_mean))

            self.tc_cats[quantity] = tc_cats
            return tc_cats

    def get_shear_cat(self, arr_data, quantity, sub_mean=False):
        if sub_mean:
            g1_mean=np.average(arr_data[quantity[0]],weights=arr_data[self.w_col])
            g2_mean=np.average(arr_data[quantity[1]],weights=arr_data[self.w_col])
            print 'subtracting mean (%f,%f)'%(g1_mean,g2_mean)
        else:
            g1_mean,g2_mean=0.,0.
        return treecorr.Catalog(g1=arr_data[quantity[0]]-g1_mean, g2=arr_data[quantity[1]]-g2_mean,ra=arr_data[self.x_col],
                                        dec=arr_data[self.y_col],ra_units=self.x_units, dec_units=self.y_units, w=arr_data[self.w_col])

    def get_m_cat(self, arr_data, quantity, sub_mean=False):
        if sub_mean:
            k_mean = np.average(arr_data[quantity],weights=arr_data[self.w_col])
        else:
            k_mean = 0.
        return treecorr.Catalog(k=arr_data[quantity]-k_mean,ra=arr_data[self.x_col],
                                        dec=arr_data[self.y_col],ra_units=self.x_units, dec_units=self.y_units, w=arr_data[self.w_col])

    def get_count_cat(self, arr_data, quantity=None, sub_mean=False):
        return treecorr.Catalog(ra=arr_data[self.x_col],
                                        dec=arr_data[self.y_col],ra_units=self.x_units, dec_units=self.y_units, w=arr_data[self.w_col])

    def redshift_split(self,zbin_col_name=None,z_bin_lims=None, zcol='z', zmin=None, zmax=None, n_split=1, kind='equal_number', ret_cats=False, append_zbin_to_arr='zbin', exclude_zbin=None, equal_bin_width=False):
        """Function to split catalog into redshift bins. Can take various inputs
        zbin_col_name : the catalog has an array which indicates the bin number for each galaxy
        zmin,zmax,n_split : split into n_split bins between zmin and zmax
        equal_bin_width : if True, use equal bin width bins, otherwise use equal number bins"""
        

        self.zbin_masks=[]  # a mask column for each redshift bin, for ease of selecting the object in that bin
        if (zmin is None) and (zmax is None) and (n_split==1) and (zbin_col_name is None) and (z_bin_lims is None):
            #No real work to do in this case
            self.zbin_column=np.zeros(len(self.arr_data))
            self.z_bin_lims=[-1,-1]
            self.zbin_masks.append(np.ones_like(self.zbin_column,dtype=bool))
            self.zbin_col_vals=[0]
            self.get_bin_stats()
            self.num_z_bins=1

        elif zbin_col_name is not None:
            #z binning already defined by this column, so again, not much to do
            self.zbin_col_name=zbin_col_name
            if exclude_zbin is not None:
                exclude=(self.arr_data[zbin_col_name]==exclude_zbin)
                keep=np.ones(self.arr_data.shape[0],dtype=bool)
                keep[exclude]=False
                self.arr_data=self.arr_data[keep]
            self.zbin_column=self.arr_data[zbin_col_name].astype(int)
            self.zbin_col_vals=np.unique(self.zbin_column)
            self.zbin_masks = [(self.arr_data[zbin_col_name]==zbin) for zbin in self.zbin_col_vals]
            self.num_z_bins=len(self.zbin_col_vals[self.zbin_col_vals>=0])
            self.get_bin_stats()
        
        else:
            if zmin is None:
                zmin=self.arr_data[zcol].min()
            if zmax is None:
                zmax=self.arr_data[zcol].max()
            if equal_bin_width:
                z_bin_lims = np.linspace(zmin, zmax, n_split+1)
            if z_bin_lims is None:
                in_range=(self.arr_data[zcol]>=zmin)*(self.arr_data[zcol]<=zmax)
                self.arr_data=self.arr_data[in_range]

                assert zcol in self.arr_data.dtype.names
                #sort array by z column
                self.arr_data=np.sort(self.arr_data, order=zcol)

                zs=self.arr_data[zcol]

                self.zbin_masks=[]
                if n_split==1:
                    self.zbin_column=np.zeros_like(zs)
                    self.z_bin_lims=[zmin,zmax]
                    self.zbin_masks.append(np.ones_like(zs,dtype=bool))
                else:
                    z_bin_inds=np.zeros(len(zs))
                    z_splits=np.array_split(zs,n_split)
                    z_los,z_his=[split[0] for split in z_splits],[split[-1] for split in z_splits]
                    for i,(z_lo,z_hi) in enumerate(zip(z_los,z_his)):
                        in_bin=(zs>=z_lo)*(zs<z_hi)
                        self.zbin_masks.append(in_bin)
                        z_bin_inds[in_bin]=i
                    z_bin_lims=z_los+[z_his[-1]]
                    self.z_bin_lims=z_bin_lims        
                    self.zbin_column = z_bin_inds

                self.zbin_col_vals=np.arange(n_split).astype(int)
                self.num_z_bins=n_split

            else:
                self.z_bin_lims=z_bin_lims
                self.num_z_bins=len(self.z_bin_lims)-1
                self.zbin_col_vals=np.arange(len(z_bin_lims)-1)
                in_range=(self.arr_data[zcol]>=z_bin_lims[0])*(self.arr_data[zcol]<=z_bin_lims[-1])
                self.arr_data=self.arr_data[in_range]
                zs=self.arr_data[zcol]
                z_bin_inds=np.zeros(len(zs))
                for ibin,(z_lo,z_hi) in enumerate(zip(z_bin_lims[:-1],z_bin_lims[1:])):
                    in_bin=(zs>=z_lo)*(zs<z_hi)
                    self.zbin_masks.append(in_bin)
                    z_bin_inds[in_bin]=ibin
                self.zbin_column = z_bin_inds

            self.get_bin_stats()

            if append_zbin_to_arr is not None:
                try:
                    self.arr_data = append_fields(self.arr_data, append_zbin_to_arr, self.zbin_column, dtypes='i4')
                except ValueError:
                    self.arr_data[append_zbin_to_arr]=self.zbin_column
                self.zbin_col_name=append_zbin_to_arr
        self.num_z_bins = len(self.zbin_col_vals)
        self.redshift_split_done=True
        if ret_cats:
            return [self.arr_data[in_bin] for in_bin in self.zbin_masks]

class CorrMeasurement(object):

    def __init__(self, gal_cat1, q1, gal_cat2=None, q2=None, rand_cat1=None, rand_cat2=None, XX=None,
                 min_sep=1., max_sep=400., nbins=10, bin_slop=1.,sep_units='arcmin', sub_mean=False):

        """
        - gal_cat1 and gal_cat2 should be GalCat objects. q1 and q2 are the quantities to be correlated e.g. 'kappa' or '('e1','e2')'.
        - gal_cat2 and q2 are optional since we may be doing an autocorrelation (q1=q2) or correlating two quantities from a single GalCat
        (gal_cat2=gal_cat1).
        - You can set q1 or q2 = 'count', to perform number count correlations (and e.g. w(theta) if you also provide random GalCats 
        as rand_cat1 and/or rand_cat2."""

        self.gal_cat1,self.q1,self.gal_cat2,self.q2,self.rand_cat1,self.rand_cat2,self.XX,self.sub_mean=gal_cat1,q1,gal_cat2,q2,rand_cat1,rand_cat2,XX,sub_mean

        self.sep_units=sep_units
        if sep_units=='arcmin':
            self.ang_units=astropy.units.arcmin
        if self.q1=='count':
            q1_spin=0
        else:
            q1_spin = gal_cat1.spin[gal_cat1.quantities.index(self.q1)]
        self.tc_cats1 = gal_cat1.get_tc_cats(self.q1, sub_mean=self.sub_mean)
        self.subpatch_inds1 = gal_cat1.get_zbin_subpatch_cols()
        if gal_cat2:
            if q2:
                if q2=='count':
                    q2_spin=0
                else:
                    q2_spin = gal_cat2.spin[gal_cat2.quantities.index(q2)]
                self.tc_cats2=gal_cat2.get_tc_cats(q2, sub_mean=sub_mean)
            else:
                self.tc_cats2=gal_cat2.get_tc_cats(q1, sub_mean=sub_mean)
                q2=q1
                q2_spin = q1_spin
            self.subpatch_inds2 = gal_cat2.get_zbin_subpatch_cols()
        else:
            self.tc_cats2 = None
            if q2:
                self.tc_cats2 = gal_cat1.get_tc_cats(q2, sub_mean=sub_mean)
                self.subpatch_inds2 = self.subpatch_inds1
                if q2=='count':
                    q2_spin=0
                else:
                    q2_spin = gal_cat1.spin[gal_cat1.quantities.index(q2)]
            else:
                q2=q1
                q2_spin=q1_spin

        #if doing count correlations, also need randoms
        self.rand_tc_cats1,self.rand_tc_cats2=None,None
        if q1=='count':
            if rand_cat1:
                self.rand_tc_cats1=rand_cat1.get_tc_cats('count')
                self.rand_tc_cats2=None
                self.rand_subpatch_inds1 = rand_cat1.get_zbin_subpatch_cols()

        if (gal_cat2 and gal_cat2!=gal_cat1):
            if q2=='count':
                if rand_cat2:
                    self.rand_tc_cats2=rand_cat2.get_tc_cats('count')
                    self.rand_subpatch_inds2 = rand_cat2.get_zbin_subpatch_cols()


        if ((q1=='count') and (q2=='count')):
            if XX is None:
                self.XX=treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, sep_units=sep_units, nbins=nbins, bin_slop=bin_slop)
            self.process_method = process_count_count
            self.types = [(twopoint.Types.galaxy_position_real,twopoint.Types.galaxy_position_real)]
        elif ((q1_spin==2) and (q2_spin==2)):
            if XX is None:
                self.XX=treecorr.GGCorrelation(min_sep=min_sep, max_sep=max_sep, sep_units=sep_units, nbins=nbins, bin_slop=bin_slop)
            self.process_method = process_shear_shear
            types_p=(twopoint.Types.galaxy_shear_plus_real,twopoint.Types.galaxy_shear_plus_real)
            types_m=(twopoint.Types.galaxy_shear_minus_real,twopoint.Types.galaxy_shear_minus_real)
            self.types = [types_p,types_m]
        elif ((q1=='count') and (q2_spin==2)):
            if XX is None:
                self.XX=treecorr.NGCorrelation(min_sep=min_sep, max_sep=max_sep, sep_units=sep_units, nbins=nbins, bin_slop=bin_slop)
            self.process_method = process_count_shear
            self.types =    [(twopoint.Types.galaxy_position_real,twopoint.Types.galaxy_shear_plus_real)]
        elif ((q2=='count') and (q1_spin==2)):
            if XX is None:
                self.XX=treecorr.NGCorrelation(min_sep=min_sep, max_sep=max_sep, sep_units=sep_units, nbins=nbins, bin_slop=bin_slop)
            self.process_method = process_count_shear
            self.types = [(twopoint.Types.galaxy_shear_plus_real,twopoint.Types.galaxy_position_real)]
        else:
            raise NotYetImpelmentedError("Could not interpret quantity pairing {0},{1}".format((q1,q2)))
        print 'process method:', self.process_method
        self.metadata = {'min_sep':self.XX.min_sep,'max_sep':self.XX.max_sep, 'bin_slop':self.XX.bin_slop}
        

    def process(self):
        corr_dict, corr_arrs = self.process_method(self.XX, self.tc_cats1, rand_tc_cats1=self.rand_tc_cats1, tc_cats2=self.tc_cats2, rand_tc_cats2=self.rand_tc_cats2)
        print 'processing weights',[t.w.sum() for t in self.tc_cats1]
        return corr_dict, corr_arrs

    def get_spec(self, name, kernel_name1, kernel_name2=None):
        
        if isinstance(name,basestring):
            name=[name]
        corr_dict, corr_arrs = self.process()
        if not kernel_name2:
            if not self.gal_cat2:
                kernel_name2=kernel_name1
            else:
                raise ValueError("supply a kernel name for second catalog")
        specs=[]
        for n,types,corr_arr in zip(name, self.types, corr_arrs):
            specs.append(twopoint.SpectrumMeasurement(n, corr_arr["zbins"].T+1, types, (kernel_name1, kernel_name2),
                                                        "SAMPLE", corr_arr["angular_bin"], corr_arr['xi'], angle=corr_arr["theta"],
                                                        angle_unit=self.sep_units, metadata=self.metadata))
        return specs

    def get_subpatch_specs(self, names, mode='subsample', subsample2=False, nsub=None):

        assert mode in ['subsample', 'jackknife']
    
        #Now loop through subpatches (and subpatch combinations if requested)
        #first get unique patch numbers and also extract original weights
        patch_nums = self.gal_cat1.patch_nums
        w1s_orig = [np.copy(g.w) for g in self.tc_cats1]
        if self.rand_cat1:
            patch_nums = np.unique(np.concatenate((patch_nums,self.rand_cat1.patch_nums)))
            rand_w1s_orig = [np.copy(g.w) for g in self.rand_tc_cats1]
        if subsample2:
            if self.gal_cat2:
                patch_nums = np.unique(np.concatenate((patch_nums,self.gal_cat2.patch_nums)))
                w2s_orig = [np.copy(g.w) for g in self.tc_cats2]
            if self.rand_cat2:
                patch_nums = np.unique(np.concatenate((patch_nums,self.rand_cat2.patch_nums)))
                rand_w2s_orig = [np.copy(g.w) for g in self.rand_tc_cats2]

        patch_corrs = []
        metadata_list = []
        patches_done=0
        for i_patch in patch_nums:
            #use weights to restrict subsamples
            if nsub:
                if patches_done>nsub:
                    continue
            patches_done+=1
            for k in range(len(self.tc_cats1)):
                w1_new = np.copy(w1s_orig[k])
                if mode == 'subsample':
                    w1_new[self.subpatch_inds1[k]!=i_patch]=0.
                elif mode == 'jackknife':
                    w1_new[self.subpatch_inds1[k]==i_patch]=0.
                self.tc_cats1[k].w = w1_new
                #print self.tc_cats1[k].gfield
                #delattr(self.tc_cats1[k], 'gfield')
                self.tc_cats1[k].gfields,self.tc_cats1[k].nfields,self.tc_cats1[k].kfields={},{},{}
                if self.rand_tc_cats1:
                    w1r_new = np.copy(rand_w1s_orig[k])
                    if mode == 'subsample':
                        w1r_new[self.rand_subpatch_inds1!=i_patch]=0.
                    elif mode== 'jackknife':
                        w1r_new[self.rand_subpatch_inds1==i_patch]=0.
                    self.rand_tc_cats1[k].w = w1r_new
                    self.rand_tc_cats1[k].gfields,self.rand_tc_cats1[k].nfields,self.rand_tc_cats1[k].kfields={},{},{}

            if self.gal_cat2:
                if subsample2:
                    for k in len(self.tc_cats2):
                        w2_new = np.copy(w2s_orig[k])
                        if mode == 'subsample':
                            w2_new[self.rand_subpatch_inds2[k]!=i_patch]=0.
                        elif mode == 'jackknife':
                            w1_new[self.subpatch_inds2[k]==i_patch]=0.
                        self.tc_cats2[k].w = w2_new
                        self.tc_cats2[k].gfields,self.tc_cats2[k].nfields,self.tc_cats2[k].kfields={},{},{}
                    if self.rand_tc_cats2:
                        w2r_new = np.copy(rand_w2s_orig[k])
                        if mode == 'subsample':
                            w2r_new[self.rand_subpatch_inds2!=i_patch]=0.
                        elif mode== 'jackknife':
                            w2r_new[self.rand_subpatch_inds2==i_patch]=0.
                        self.rand_tc_cats2[k].w = w2r_new
                        self.rand_tc_cats2[k].gfields,self.rand_tc_cats2[k].nfields,self.rand_tc_cats2[k].kfields={},{},{}

            specs = self.get_spec(names, 'NZ_DUMMY', 'NZ_DUMMY')
            #reset weights
            patch_corrs.append(specs) #just take xi+
        for i in range(len(self.tc_cats1)):
            self.tc_cats1[i].w = w1s_orig[i]
        return patch_corrs
        

def process_count_count(nn, tc_cats1, rand_tc_cats1, tc_cats2=None, rand_tc_cats2=None):

    nbin1 = len(tc_cats1)
    auto=True
    if ((tc_cats2 is not None) and (tc_cats2!=tc_cats1)):
        auto=False
        nbin2 = len(tc_cats2)
    else:
        tc_cats2=tc_cats1
        rand_tc_cats2=rand_tc_cats1    

    #Build rr and rd
    corr_count_count = treecorr_correlation2dict(nn)
    if rand_tc_cats1:
        rr=treecorr.NNCorrelation(corr_count_count)
        rd=treecorr.NNCorrelation(corr_count_count)
        dr=treecorr.NNCorrelation(corr_count_count)

    if auto:
        l_data_vec=nbin1*(nbin1+1)*corr_count_count['nbins']/2
    else:
        l_data_vec=nbin1*nbin2*corr_count_count['nbins']
    xi_array = np.zeros(l_data_vec)
    theta_array = np.zeros_like(xi_array)
    zbins_array = np.zeros(l_data_vec, dtype=(int,2))
    npairs_array= np.zeros_like(xi_array)
    angular_bin_array = np.zeros_like(xi_array,dtype=int)                                                      
    corr_ind=0
    for i in range(nbin1):
        if auto:
            jbins = range(i,nbin1)
        else:
            jbins = range(nbin2)
        for j in jbins:
            arr_inds=np.arange(corr_ind*nn.nbins,(corr_ind+1)*nn.nbins)
            corr_ind+=1
            corr_count_count[(i,j)]={}
            nn.process(tc_cats1[i],tc_cats2[j])
            corr_count_count[(i,j)]['npairs']=np.copy(nn.npairs)
            corr_count_count[(i,j)]['theta']=np.exp(np.copy(nn.meanlogr))
            #do random bits
            if rand_tc_cats1:
                rr.process(rand_tc_cats1[i],rand_tc_cats2[j])
                rd.process(rand_tc_cats1[i],tc_cats2[j])
                dr.process(tc_cats1[i],rand_tc_cats2[j])
                xi, varxi = nn.calculateXi(rr, dr, rd)
                corr_count_count[(i,j)]['xi']=np.copy(xi)
                corr_count_count[(i,j)]['varxi']=np.copy(varxi)
            else:
                xi = corr_count_count[(i,j)]['npairs']

            xi_array[arr_inds]=np.copy(xi)
            theta_array[arr_inds]=corr_count_count[(i,j)]['theta']
            npairs_array[arr_inds]=np.copy(nn.npairs)
            zbins_array[arr_inds]=(i,j)
            angular_bin_array[arr_inds]=np.arange(nn.nbins) 

    count_count_arrays={"xi":xi_array,
              "theta":theta_array,
              "npairs":npairs_array,
              "zbins":zbins_array,
              "angular_bin":angular_bin_array
        }
    return corr_count_count, [count_count_arrays]


def process_shear_shear(GG, tc_cats1, tc_cats2=None, do_nbc_m=False, rand_tc_cats1=None, rand_tc_cats2=None):

    nbin1 = len(tc_cats1)
    auto=True
    if ((tc_cats2 is not None) and (tc_cats2!=tc_cats1)):
        auto=False
        nbin2 = len(tc_cats2)
    else:
        tc_cats2=tc_cats1

    corr_shear_shear = treecorr_correlation2dict(GG)
    if do_nbc_m:
        'doing nbc'
        self.KK=treecorr.KKCorrelation(min_sep=corr_shear_shear['min_sep'], max_sep=corr_shear_shear['max_sep'],
                                       nbins=corr_shear_shear['nbins'], sep_units=corr_shear_shear['sep_units'],
                                       bin_slop=corr_shear_shear['bin_slop'])
    if auto:
        l_data_vec=nbin1*(nbin1+1)*corr_shear_shear['nbins']/2
    else:
        l_data_vec=nbin1*nbin2*corr_shear_shear['nbins']
    xip_array = np.zeros(l_data_vec)
    xim_array = np.zeros_like(xip_array)
    theta_array = np.zeros_like(xip_array)
    zbins_array = np.zeros(l_data_vec, dtype=(int,2))
    npairs_array= np.zeros_like(xip_array)
    angular_bin_array = np.zeros_like(xip_array,dtype=int)                                                      
    corr_ind=0
    for i in range(nbin1):
        if auto:
            jbins = range(i,nbin1)
        else:
            jbins = range(nbin2)
        for j in jbins:
            arr_inds=np.arange(corr_ind*GG.nbins,(corr_ind+1)*GG.nbins)
            corr_ind+=1
            corr_shear_shear[(i,j)]={}
            GG.process(tc_cats1[i],tc_cats2[j])
            theta,xip,xim,varxi=np.exp(np.copy(GG.meanlogr)),np.copy(GG.xip),np.copy(GG.xim),np.copy(GG.varxi)
            if do_nbc_m:
                corr_shear_shear[(i,j)]['xip_no_nbc']=np.copy(xip)
                corr_shear_shear[(i,j)]['xim_no_nbc']=np.copy(xim)
                self.KK.process(self.m_treecorr_cats[i],self.m_treecorr_cats[j])
                xi_mm=np.copy(self.KK.xi)
                xip/=xi_mm
                xim/=xi_mm
                varxi/=xi_mm
            corr_shear_shear[(i,j)]['theta']=theta
            corr_shear_shear[(i,j)]['xip']=xip
            corr_shear_shear[(i,j)]['xim']=xim
            corr_shear_shear[(i,j)]['varxi']=varxi
            xip_array[arr_inds]=xip
            xim_array[arr_inds]=xim
            theta_array[arr_inds]=theta
            npairs_array[arr_inds]=np.copy(GG.npairs)
            zbins_array[arr_inds]=(i,j)
            angular_bin_array[arr_inds]=np.arange(GG.nbins) 

        xip_arrays={"xi":xip_array,
              "theta":theta_array,
              "npairs":npairs_array,
              "zbins":zbins_array,
              "angular_bin":angular_bin_array
        }
        xim_arrays={"xi":xim_array,
              "theta":theta_array,
              "npairs":npairs_array,
              "zbins":zbins_array,
              "angular_bin":angular_bin_array
        }

    return corr_shear_shear, [xip_arrays,xim_arrays]

def process_count_shear(nG, tc_cats1, tc_cats2, rand_tc_cats1=None, do_nbc_m=False, rand_tc_cats2=None):
    #build rr and rd
    corr_count_shear = treecorr_correlation2dict(nG)
    if rand_tc_cats1:
        rG = treecorr.NGCorrelation(corr_count_shear)
    if do_nbc_m:
        self.nK=treecorr.NKCorrelation(min_sep=corr_count_shear['min_sep'], max_sep=corr_count_shear['max_sep'],
                                       nbins=corr_count_shear['nbins'], sep_units=corr_count_shear['sep_units'],
                                       bin_slop=corr_count_shear['bin_slop'])

    nbin1,nbin2 = len(tc_cats1), len(tc_cats2)
    l_data_vec=nbin1*nbin2*nG.nbins
    xi_array = np.zeros(l_data_vec)
    xix_array = np.zeros_like(xi_array)
    theta_array = np.zeros_like(xi_array)
    zbins_array = np.zeros(l_data_vec, dtype=(int,2))
    npairs_array= np.zeros_like(xi_array)
    angular_bin_array = np.zeros_like(xi_array,dtype=int)                                                      
    corr_ind=0
    for i in range(nbin1):
        for j in range(nbin2):
            arr_inds=np.arange(corr_ind*nG.nbins,(corr_ind+1)*nG.nbins)
            corr_ind+=1
            corr_count_shear[(i,j)]={}
            nG.process(tc_cats1[i], tc_cats2[j])
            corr_count_shear[(i,j)]['npairs']=np.copy(nG.npairs)
            corr_count_shear[(i,j)]['theta']=np.exp(np.copy(nG.meanlogr))
            gt,gx,varxi=np.copy(nG.xi),np.copy(nG.xi_im),np.copy(nG.varxi)
            if rand_tc_cats1 is not None:
                nG.process(rand_tc_cats1[i],tc_cats2[j])
                gt-=np.copy(nG.xi)
                gx-=np.copy(nG.xi_im)
                varxi+=np.copy(nG.varxi)
            if do_nbc_m:
                self.nK.process(self.pos_treecorr_cats[i],self.m_treecorr_cats[j])
                nm=np.copy(self.nK.xi)
                corr_count_shear[(i,j)]['xi_no_nbc']=np.copy(gt)
                corr_count_shear[(i,j)]['xi_im_no_nbc']=np.copy(gx)
                gt/=nm
                gx/=nm
                varxi/=nm                    
            corr_count_shear[(i,j)]['xi']=gt
            corr_count_shear[(i,j)]['xi_im']=gx
            corr_count_shear[(i,j)]['varxi']=varxi
            xi_array[arr_inds]=gt
            theta_array[arr_inds]=corr_count_shear[(i,j)]['theta']
            npairs_array[arr_inds]=np.copy(nG.npairs)
            zbins_array[arr_inds]=(i,j)
            angular_bin_array[arr_inds]=np.arange(nG.nbins) 
    count_shear_arrays={"xi":xi_array,
              "theta":theta_array,
              "npairs":npairs_array,
              "zbins":zbins_array,
              "angular_bin":angular_bin_array
        }
    return corr_count_shear, [count_shear_arrays]

def process_kappa_kappa(KK, tc_cats1, tc_cats2=None, do_nbc_m=False, rand_tc_cats1=None, rand_tc_cats2=None):

    nbin1 = len(tc_cats1)
    auto=True
    if ((tc_cats2 is not None) and (tc_cats2!=tc_cats1)):
        auto=False
        nbin2 = len(tc_cats2)
    else:
        tc_cats2=tc_cats1

    corr_kappa_kappa = treecorr_correlation2dict(KK)
    if auto:
        l_data_vec=nbin1*(nbin1+1)*corr_kappa_kappa['nbins']/2
    else:
        l_data_vec=nbin1*nbin2*corr_kappa_kappa['nbins']
    xi_array = np.zeros(l_data_vec)
    theta_array = np.zeros_like(xi_array)
    zbins_array = np.zeros(l_data_vec, dtype=(int,2))
    npairs_array= np.zeros_like(xi_array)
    angular_bin_array = np.zeros_like(xi_array,dtype=int)                                                      
    corr_ind=0
    for i in range(nbin1):
        if auto:
            jbins = range(i,nbin1)
        else:
            jbins = range(nbin2)
        for j in jbins:
            arr_inds=np.arange(corr_ind*KK.nbins,(corr_ind+1)*KK.nbins)
            corr_ind+=1
            corr_kappa_kappa[(i,j)]={}
            KK.process(tc_cats1[i],tc_cats2[j])
            theta,xi,varxi=np.exp(np.copy(KK.meanlogr)),np.copy(KK.xi),np.copy(KK.varxi)
            corr_kappa_kappa[(i,j)]['theta']=theta
            corr_kappa_kappa[(i,j)]['xi']=xi
            corr_kappa_kappa[(i,j)]['varxi']=varxi
            xi_array[arr_inds]=xi
            theta_array[arr_inds]=theta
            npairs_array[arr_inds]=np.copy(KK.npairs)
            zbins_array[arr_inds]=(i,j)
            angular_bin_array[arr_inds]=np.arange(KK.nbins) 

        xi_arrays={"xi":xi_array,
              "theta":theta_array,
              "npairs":npairs_array,
              "zbins":zbins_array,
              "angular_bin":angular_bin_array
        }

    return corr_kappa_kappa, [xi_arrays]


def process_kappa_shear(KG, tc_cats1, tc_cats2, rand_tc_cats1=None, do_nbc_m=False, rand_tc_cats2=None):

    corr_kappa_shear = treecorr_correlation2dict(KG)
    nbin1,nbin2 = len(tc_cats1), len(tc_cats2)
    l_data_vec=nbin1*nbin2*KG.nbins
    xi_array = np.zeros(l_data_vec)
    xix_array = np.zeros_like(xi_array)
    theta_array = np.zeros_like(xi_array)
    zbins_array = np.zeros(l_data_vec, dtype=(int,2))
    npairs_array= np.zeros_like(xi_array)
    angular_bin_array = np.zeros_like(xi_array,dtype=int)                                                      
    corr_ind=0
    for i in range(nbin1):
        for j in range(nbin2):
            arr_inds=np.arange(corr_ind*KG.nbins,(corr_ind+1)*KG.nbins)
            corr_ind+=1
            corr_kappa_shear[(i,j)]={}
            KG.process(tc_cats1[i], tc_cats2[j])
            corr_kappa_shear[(i,j)]['npairs']=np.copy(KG.npairs)
            corr_kappa_shear[(i,j)]['theta']=np.exp(np.copy(KG.meanlogr))
            xi,xi_im,varxi=np.copy(KG.xi),np.copy(KG.xi_im),np.copy(KG.varxi)
            corr_kappa_shear[(i,j)]['xi']=xi
            corr_kappa_shear[(i,j)]['xi_im']=xi_im
            corr_kappa_shear[(i,j)]['varxi']=varxi
            xi_array[arr_inds]=xi
            theta_array[arr_inds]=corr_kappa_shear[(i,j)]['theta']
            npairs_array[arr_inds]=np.copy(KG.npairs)
            zbins_array[arr_inds]=(i,j)
            angular_bin_array[arr_inds]=np.arange(KG.nbins) 
    kappa_shear_arrays={"xi":xi_array,
              "theta":theta_array,
              "npairs":npairs_array,
              "zbins":zbins_array,
              "angular_bin":angular_bin_array
        }
    return corr_kappa_shear, [kappa_shear_arrays]
