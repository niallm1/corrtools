import numpy as np

def sample_cov(xi_arrays,mode='full'):
    """mode should be full, subsample or jk"""
    nsample,npoints=xi_arrays.shape
    Cov=np.zeros((npoints,npoints))
    Corr=np.zeros_like(Cov)

    xi_mean=np.mean(xi_arrays, axis=0)
    for i in range(npoints):
        for j in range(npoints):
            Cov[i,j]=np.sum((xi_arrays[:,i]-xi_mean[i])*(xi_arrays[:,j]-xi_mean[j]))/nsample
    #This is the covariance in a patch of size A/nsample. Assume Cov ~ 1/A so Cov=Cov/nsample
    if mode=='subsample':
        Cov /= nsample
    elif mode=="jk":
        Cov *= (nsample-1)
    for i in range(npoints):
        for j in range(npoints):
            Corr[i,j] = Cov[i,j]/np.sqrt(Cov[i,i]*Cov[j,j])  
    return Cov, Corr

def comb_cov(xi_arrays_list,mode='full'):
    comb_arrays=[]
    for i in range(len(xi_arrays_list[0])):
        comb_arrays.append(np.concatenate([xi_arr[i] for xi_arr in xi_arrays_list]))

    return sample_cov(np.array(comb_arrays),mode=mode)
